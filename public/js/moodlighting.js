var host = "http://"+window.location.host;
console.log("host: "+host);

$( document ).ready(function() {
	console.log( "ready!" );

	// set switch state
	getPowerswitchState();
	setInterval(getPowerswitchState, 700);
	// update sensor data every second
	updateSensorData();
	setInterval(updateSensorData, 2000);


	getFeaturesData();
	setInterval(getFeaturesData, 700);

	$('#liveViewModal').on('shown.bs.modal', function (e) {

		if( true ){
			$('#livestreamBox').html('<img src="'+window.location.origin+':8081" width="100%">');
		} else {
			$('#livestreamBox').html('<img src="pictures/default.jpg" width="100%">');
		}
	});

	// state switch
	$('#switch1').change(function() {
		if( $('#switch1').prop('checked') ){
			setPowerswitchChannel1( "on" );
		}
		else{
			setPowerswitchChannel1( "off" );
		}
	});

	$('#switch2').change(function() {
		if( $('#switch2').prop('checked') ){
			setPowerswitchChannel2( "on" );
		}
		else{
			setPowerswitchChannel2( "off" );
		}
	});

		$('#switch3').change(function() {
			if( $('#switch3').prop('checked') ){
				setPowerswitchChannel3( "on" );
			}
			else{
				setPowerswitchChannel3( "off" );
			}
		});

		$('#switch4').change(function() {
			if( $('#switch4').prop('checked') ){
				setPowerswitchChannel4( "on" );
			}
			else{
				setPowerswitchChannel4( "off" );
			}
		});

		$('#switchDusk').change(function() {
			if( $('#switchDusk').prop('checked') ){
				setDuskSwitch( "on" );
			}
			else{
				setDuskSwitch( "off" );
			}
		});

		$('#switchMotionLight').change(function() {
			if( $('#switchMotionLight').prop('checked') ){
				setMotionLight( "on" );
			}
			else{
				setMotionLight( "off" );
			}
		});
});

/*	API functions	*/
function updateSensorData(){
	$.getJSON(host+'/api/sensordata', function(data) {
		if( data.status == 'connected' ){
			// make display fields visible
			$('.sensordata').css("color","#2B2D42");

			$('#temperatureDisplay').html("<i class=\"fa fa-thermometer-quarter\"></i> "+parseFloat(data.temperature)+"&deg;C");
			$('#humidityDisplay').html("<i class=\"fa fa-tint\"></i> "+parseFloat(data.humidity)+"%");
			$('#lightingDisplay').html("<i class=\"fa fa-lightbulb-o\"></i> "+parseFloat(data.lighting)+"%");
		}
		else{
			// gray out all display fields
			$('.sensordata').css("color","#8D99AE");
			$('#temperatureDisplay').html("<i class=\"fa fa-thermometer-quarter\"></i> xx.x");
			$('#humidityDisplay').html("<i class=\"fa fa-tint\"></i> xx.x");
			$('#lightingDisplay').html("<i class=\"fa fa-lightbulb-o\"></i> xx.x");
		}
	});
}

function setPowerswitchChannel1( state ){
	var data = {};
	data.state = state;

	$.ajax( {
        type: "POST",
        url: host+'/api/powerswitch/channel_1',
        dataType: 'json',
        data: JSON.stringify(data),
        success: null,
	contentType: 'application/json'
    });
}

function setPowerswitchChannel2( state ){
	var data = {};
	data.state = state;

	$.ajax( {
        type: "POST",
        url: host+'/api/powerswitch/channel_2',
        dataType: 'json',
        data: JSON.stringify(data),
        success: null,
	contentType: 'application/json'
    });
}

function setPowerswitchChannel3( state ){
	var data = {};
	data.state = state;

	$.ajax( {
        type: "POST",
        url: host+'/api/powerswitch/channel_3',
        dataType: 'json',
        data: JSON.stringify(data),
        success: null,
	contentType: 'application/json'
    });
}

function setPowerswitchChannel4( state ){
	var data = {};
	data.state = state;

	$.ajax( {
        type: "POST",
        url: host+'/api/powerswitch/channel_4',
        dataType: 'json',
        data: JSON.stringify(data),
        success: null,
	contentType: 'application/json'
    });
}

function getPowerswitchState(){
	$.getJSON(host+'/api/powerswitchdata', function(data) {
		console.log(data);

		if( data.channel_1_state == "on" ){
			$('#switch1').prop('checked', true);
		}
		else if( data.channel_1_state == "off" ){
			$('#switch1').prop('checked', false);
		}

		if( data.channel_2_state == "on" ){
			$('#switch2').prop('checked', true);
		}
		else if( data.channel_2_state == "off" ){
			$('#switch2').prop('checked', false);
		}

		if( data.channel_3_state == "on" ){
			$('#switch3').prop('checked', true);
		}
		else if( data.channel_3_state == "off" ){
			$('#switch3').prop('checked', false);
		}

		if( data.channel_4_state == "on" ){
			$('#switch4').prop('checked', true);
		}
		else if( data.channel_4_state == "off" ){
			$('#switch4').prop('checked', false);
		}
	});
}

function setDuskSwitch( state ){
	var data = {};
	data.state = state;

	$.ajax( {
        type: "POST",
        url: host+'/api/features/duskSwitch',
        dataType: 'json',
        data: JSON.stringify(data),
        success: null,
				contentType: 'application/json'
    });
	}

	function setMotionLight( state ){
		var data = {};
		data.state = state;

		$.ajax( {
	        type: "POST",
	        url: host+'/api/features/motionLight',
	        dataType: 'json',
	        data: JSON.stringify(data),
	        success: null,
					contentType: 'application/json'
	    });
		}

	function getFeaturesData(){
		$.getJSON(host+'/api/features/duskSwitch', function(data) {
			console.log(data);

			if( data.duskSwitch_state == "on" ){
				$('#switchDusk').prop('checked', true);
			}
			else if( data.duskSwitch_state == "off" ){
				$('#switchDusk').prop('checked', false);
			}
		});

		$.getJSON(host+'/api/features/motionLight', function(data) {
			console.log(data);

			if( data.motionlight_state == "on" ){
				$('#switchMotionLight').prop('checked', true);
			}
			else if( data.duskSwitch_state == "off" ){
				$('#switchMotionLight').prop('checked', false);
			}

			// TODO don't update all the time, only on change
			getMotionImagesList();
		});
}

function getMotionImagesList(){
	$.getJSON(host+'/api/features/motionImages', function(data) {
		// clear current list
		$('#motionPictures').html("");
		// print every element's picture and date
		data.motionImages.forEach(function(motionElement, index){
			var htmlString = new String();
			htmlString = htmlString.concat('<div class="col-6">');
			htmlString = htmlString.concat('<div class="motionImageMenu"><a type="button" onclick="deleteImage( \''+motionElement.uuid+'\' );" type="button" class="btn btn-sm btn-outline-dark" style="font-size:20px; margin:2px;"><i class="fa fa-trash"></i></a>');
			if( motionElement.saved ){	// make star yellow if saved, and unsave if clicked
				htmlString = htmlString.concat('<a onclick="unsaveImage( \''+motionElement.uuid+'\' )" type="button" class="btn btn-sm btn-outline-dark" style="color: yellow; font-size:20px; margin:2px;"><i class="fa fa-star"></i></a>');
			} else {										// otherwise star outline, and save if clicked
				htmlString = htmlString.concat('<a onclick="saveImage( \''+motionElement.uuid+'\' )" type="button" class="btn btn-sm btn-outline-dark" style="font-size:20px; margin:2px;"><i class="fa fa-star-o"></i></a>');
			}
			htmlString = htmlString.concat('<a onclick="setMovieToModal(\''+motionElement.moviePath+'\');" type="button" class="btn btn-sm btn-outline-dark" style="font-size:20px; margin:2px;"><i class="fa fa-film"></i></a>');

			htmlString = htmlString.concat('</div><img class="motionImage" src="'+motionElement.imagePath+'" width="100%" style="margin:2px;"></br>'+motionElement.imageDescription+'</div>');
			$('#motionPictures').append( htmlString );
		});
	});
}

function saveImage( uuid ){
	var data = {};
	data.uuid = uuid;

	$.ajax( {
  	type: "POST",
    url: host+'/api/pictures/save',
    dataType: 'json',
    data: JSON.stringify(data),
    success: null,
		contentType: 'application/json'
	});
}

function unsaveImage( uuid ){
	var data = {};
	data.uuid = uuid;

	$.ajax( {
  	type: "POST",
    url: host+'/api/pictures/unsave',
    dataType: 'json',
    data: JSON.stringify(data),
    success: null,
		contentType: 'application/json'
	});
}

function deleteImage( uuid ){
	var data = {};
	data.uuid = uuid;

	$.ajax( {
  	type: "POST",
    url: host+'/api/pictures/delete',
    dataType: 'json',
    data: JSON.stringify(data),
    success: null,
		contentType: 'application/json'
	});
}

$('#shutdownButton').click(function() {
	$.ajax( {
  	type: "POST",
    url: host+'/api/system/shutdown',
    dataType: 'json',
    data: null,
    success: null,
		contentType: 'application/json'
	});
});

$('#rebootButton').click(function() {
	$.ajax( {
  	type: "POST",
    url: host+'/api/system/reboot',
    dataType: 'json',
    data: null,
    success: null,
		contentType: 'application/json'
	});
});

$('#swupdateButton').click(function() {
	window.alert("TODO: implement software update!");
});

function setMovieToModal( moviePath ){
	var htmlString = "";
	$('#livestreamBox').html(""); // clear movie box	htmlString
	htmlString = htmlString.concat('<video width="100%" muted poster autoplay>');
	htmlString = htmlString.concat('<source src="'+moviePath+'" type="video/mp4">');
	htmlString = htmlString.concat('</video>');

	$('#livestreamBox').append(htmlString);
}
