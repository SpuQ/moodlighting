const qdevice = require('qdevice');
const express = require('express');
const fs = require('fs');
var bodyParser = require('body-parser');
const app = express();
const qsystem = require('qsystem');


app.use(express.static('public'));
app.use(bodyParser.json());
app.listen(80, () => console.log('Mood Lighting server listening on port 80!'));

// system
var system = new qsystem();

system.on('reboot', function(){
	cleanup();
});
system.on('shutdown', function(){
	cleanup();
});

// create hardware devices
var sensor = new qdevice("freyaSensor_1");
var powerSwitch = new qdevice("RelayModule_1");

moodLightInit();

/* Qcom Temperature/RHumidity/RLighting sensor */
var sensorTemperature = 0.0;
var sensorHumidity = 0.0;
var sensorLighting = 0.0;
var sensorStatus = "";

sensor.on('data', function(data){
	if( data.signal == "humidity" ){
		sensorHumidity = data.argument;
	}
	else if( data.signal == "lighting" ){
		sensorLighting = data.argument;
	}
	else if (data.signal == "temperature" ){
		sensorTemperature = data.argument;
	}
});

sensor.on('connected', function(){
	sensorStatus = 'connected';
	console.log("sensor connected");
});

sensor.on('disconnected', function(){
	sensorStatus = 'disconnected';
	sensorTemperature = 0.0;
	sensorHumidity = 0.0;
	sensorLighting = 0.0;
	console.log("sensor disconnected");
});

/* Qcom 4-channel Power Switch */
var powerSwitchStatus = "";
var powerSwitch_channel_1_state;
var powerSwitch_channel_2_state;
var powerSwitch_channel_3_state;
var powerSwitch_channel_4_state;

powerSwitch.on('connected', function(){
	powerSwitchStatus = 'connected';
	// ToDo reload last switch states instead of just resetting?
	powerSwitch_init();
	console.log("powerSwitch connected");
});

powerSwitch.on('disconnected', function(){
	powerSwitchStatus = 'disconnected';
	console.log("powerSwitch disconnected");
});

/*	get sensor data	*/
app.get('/api/sensordata', function (req, res){
	var data = {};
	data.temperature = sensorTemperature;
	data.humidity = sensorHumidity;
	data.lighting = sensorLighting;
	data.status = sensorStatus;
	res.send(data);
});

/*	get the current state of the moodlight	*/
app.get('/api/powerswitchdata', function (req, res){
	var data = {};
	data.status = powerSwitchStatus;
	data.channel_1_state = powerSwitch_channel_1_state;
	data.channel_2_state = powerSwitch_channel_2_state;
	data.channel_3_state = powerSwitch_channel_3_state;
	data.channel_4_state = powerSwitch_channel_4_state;
	res.send(data);
});

/*	change the current state of the moodlight	*/
app.post('/api/powerswitch/channel_1', function(req, res){
	if(typeof req.body.state !== 'undefined' ){
		if(req.body.state == "on"){
			powerSwitch_channel_1_on();
		}
		else if(req.body.state == "off"){
			powerSwitch_channel_1_off();
			// turn off duskSwitch
			duskSwitch_off();
		}
	}
	res.send(req.body);
});

/*	change the current state of the moodlight2	*/
app.post('/api/powerswitch/channel_2', function(req, res){
	if(typeof req.body.state !== 'undefined' ){
		if(req.body.state == "on"){
			powerSwitch_channel_2_on();
		}
		else if(req.body.state == "off"){
			powerSwitch_channel_2_off();
		}
	}
	res.send(req.body);
});

/*	change the current state of the moodlight	*/
app.post('/api/powerswitch/channel_3', function(req, res){
	if(typeof req.body.state !== 'undefined' ){
		if(req.body.state == "on"){
			powerSwitch_channel_3_on();
		}
		else if(req.body.state == "off"){
			powerSwitch_channel_3_off();
		}
	}
	res.send(req.body);
});

/*	change the current state of the moodlight2	*/
app.post('/api/powerswitch/channel_4', function(req, res){
	if(typeof req.body.state !== 'undefined' ){
		if(req.body.state == "on"){
			powerSwitch_channel_4_on();
		}
		else if(req.body.state == "off"){
			powerSwitch_channel_4_off();
		}
	}
	res.send(req.body);
});

/*	duskswitch	*/
app.post('/api/features/duskSwitch', function(req, res){
	if(typeof req.body.state !== 'undefined' ){
		if(req.body.state == "on"){
			duskSwitch_on();
		}
		else if(req.body.state == "off"){
			duskSwitch_off();
		}
	}
	res.send(req.body);
});

app.get('/api/features/duskSwitch', function (req, res){
	var data = {};
	data.duskSwitch_state = duskSwitch_state;
	res.send(data);
});

/*	duskswitch	*/
app.post('/api/features/motionLight', function(req, res){
	if(typeof req.body.state !== 'undefined' ){
		if(req.body.state == "on"){
			motionlight_on();
		}
		else if(req.body.state == "off"){
			motionlight_off();
		}
	}
	res.send(req.body);
});

app.get('/api/features/motionLight', function (req, res){
	var data = {};
	data.motionlight_state = motionlight_state;
	res.send(data);
});

app.get('/api/features/motionImages', function (req, res){
	// TODO - this is dummy data, replace with some real data
	var data = {};
	data.motionImages = [];
	data.motionImages = motion.getPictureList();
	res.send(data);
});

app.post('/api/pictures/delete', function(req, res){
	if(typeof req.body.uuid !== 'undefined' ){
		motion.deletePictureById( req.body.uuid );
	}
	res.send(req.body);
});

app.post('/api/pictures/save', function(req, res){
	if(typeof req.body.uuid !== 'undefined' ){
		motion.savePictureById( req.body.uuid );
	}
	res.send(req.body);
});

app.post('/api/pictures/unsave', function(req, res){
	if(typeof req.body.uuid !== 'undefined' ){
		motion.unsavePictureById( req.body.uuid );
	}
	res.send(req.body);
});

app.post('/api/system/reboot', function(req, res){
	system.reboot();
	res.send(req.body);
});

app.post('/api/system/shutdown', function(req, res){
	system.shutdown();
	res.send(req.body);
});

/*
 *	moodLight functions
 */

function moodLightInit(){
	console.log("Initializing Mood Lighting");
}

function powerSwitch_init(){
	// turn all channels off, just to be sure
	powerSwitch_channel_1_off();
	powerSwitch_channel_2_off();
	powerSwitch_channel_3_off();
	powerSwitch_channel_4_off();
}

function powerSwitch_channel_1_on(){
	//console.log("Turning on channel 1");
	powerSwitch.send('CH1', 'on');
	powerSwitch_channel_1_state = "on";
}

function powerSwitch_channel_1_off(){
	//console.log("Turning off channel 1");
	powerSwitch.send('CH1', 'off');
	powerSwitch_channel_1_state = "off";
}

function powerSwitch_channel_2_on(){
	//console.log("Turning on channel 2");
	powerSwitch.send('CH2', 'on');
	powerSwitch_channel_2_state = "on";
}

function powerSwitch_channel_2_off(){
	//console.log("Turning off channel 2");
	powerSwitch.send('CH2', 'off');
	powerSwitch_channel_2_state = "off";
}

function powerSwitch_channel_3_on(){
	//console.log("Turning on channel 3");
	powerSwitch.send('CH3', 'on');
	powerSwitch_channel_3_state = "on";
}

function powerSwitch_channel_3_off(){
	//console.log("Turning off channel 3");
	powerSwitch.send('CH3', 'off');
	powerSwitch_channel_3_state = "off";
}

function powerSwitch_channel_4_on(){
	//console.log("Turning on channel 4");
	powerSwitch.send('CH4', 'on');
	powerSwitch_channel_4_state = "on";
}

function powerSwitch_channel_4_off(){
	//console.log("Turning off channel 4");
	powerSwitch.send('CH4', 'off');
	powerSwitch_channel_4_state = "off";
}

// duskswitch
// turns on moodlight when it gets dark
var duskSwitch_state = 'off';
var duskSwitchInterval= 0;
var duskValuePercent = 15;

function duskSwitch_on(){
	console.log("turning on duskSwitch");
	duskSwitch_state = 'on';

	duskSwitchInterval = setInterval( function(){
		if( sensorLighting < duskValuePercent ){
			powerSwitch_channel_1_on();
		}
		else if( sensorLighting >= duskValuePercent+5 ) {
			powerSwitch_channel_1_off();
		}
	}, 5000);
}

function duskSwitch_off(){
	console.log("turning off duskSwitch");
	duskSwitch_state = 'off';
	clearInterval( duskSwitchInterval );
	duskSwichTimer = 0;
}

// Motion detection, added 18/01/2021
// moved to motion.js on 23/01/2021
const motionjs = require('qmotionjs');
var motionlight_state = "off";

var motion = new motionjs();
motion.setGalleryMaximum( 40 );
motion.startMotionDetection();		// 'motion' always on

function motionlight_on(){
	motionlight_state = "on";
}

function motionlight_off(){
	motionlight_state = "off";
}

motion.on('motion start', function(){
	if( motionlight_state == "on" ){
		if( sensorStatus == "connected" ){					// if the sensor is connected
			if( sensorLighting < duskValuePercent ){	// turn on the lights only if it's dark
				console.debug('MOTION! - dark enough ('+sensorLighting+'<'+duskValuePercent+'): turning on lights');
				powerSwitch_channel_1_on();
			}
			else {
				console.debug('MOTION! - not dark enough ('+sensorLighting+'>='+duskValuePercent+'): not turning on lights');
			}
		}
		else {																			// otherwise just turn on the lights
			console.debug('MOTION! - no sensor connected: turning on moodlight');
			powerSwitch_channel_1_on();
		}
	}
});

motion.on('motion stop', function(){
	if( motionlight_state == 'on' ){
		console.debug('motion stopped; turning off moodlight');
		powerSwitch_channel_1_off();		// turn off the lights
	}
});

// everything for a graceful shutdown
function cleanup(){
	motion.cleanup();
	console.debug('bye!');
}
// when asked to quit by the system
process.on('SIGTERM', function(){
	cleanup();
	process.exit(0);
});
// when interrupted by user (ctrl+C)
process.on('SIGINT', function(){
	cleanup();
	process.exit(0);
});
